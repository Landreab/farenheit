//Created by: Brennin Landrean
// Landreab
// 991493489
//landreab@sheridancollege.ca


package sheridan;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		int celsius = 5;
		int faren = Farenheit.fromCelsius(celsius);
		assertEquals(faren, 41);
		
	}
	@Test (expected = NumberFormatException.class)
	public void testFromCelsiusExceptio() {
		int celsius = -30;
		int faren = Farenheit.fromCelsius("wrong input");
		fail("Invalid number");
		
	}
	@Test
	public void testFromCelsiusBoundIn() {
		int celsius = 1;
		int faren = Farenheit.fromCelsius(celsius);
		assertEquals(faren, 34);
		
	}
	@Test
	public void testFromCelsiusBoundOut() {
		int celsius = -1;
		int faren = Farenheit.fromCelsius(celsius);
		assertEquals(faren, 30);
		
	}
}
